/*jshint laxcomma:true, smarttabs: true */
"use strict";
/**
 * module for creating valid equations for graphing application
 * @module module:lib/equation
 * @author Eric Satterwhite
 * @requires util
 **/


var util      = require("util")
   , events   = require("events")
   , os       = require("os")
   , defaults = require('../conf/defaults')
   , Equation
   ;
/**
 * creates an expresion procedure using the supplied equation body
 * @param {String} body The body of the function to execute, sans return statement
 * @param {String} varaible The name of the variable contain with in the equation body
 * @return equation The final euqation to use in the graghing equation
 **/
exports.from = function( text, variable ){
	"use strict"; 

	var fn       // the function that will be returned
	  , is_valid // a boolean value if the equation returns a valid response
	  , result
	  ;

	text     = util.format( "return %s", text );
	variable = variable || "x" ;
	fn       = new Function( variable, text );
	result   = fn.call( null, 1 )
	is_valid = typeof result === 'number' && !isNaN( result );

	if( is_valid ){
		return fn;
	}
		
	throw "EquationError: equations must return a numeric value"
};


exports.render = function( eq, x, y, mx, my ){
	eq = exports.from( eq );
	mx = mx || 0;
	my = my || 0;
	for( var i=mx; i<x; i++){
		console.log(eq(i))
	}
}


/**
 * DESCRIPTION
 * @class module:NAME.Thing
 * @param {TYPE} NAME DESCRIPTION
 * @example var x = new NAME.Thing({});
 */
Equation = function( body, options )/** @lends module:NAME.Thing.prototype */{
	options     = options || {};
	this._body  = body
	this._eq    = exports.from( body );
	this.x      = parseInt( options.x, 10)
	this.y      = parseInt( options.y, 10)

	this.height = parseInt( options.height || defaults.height, 10)
	this.width  = parseInt( options.width  || defaults.width, 10)

	this.minX   = parseInt( options.minX || defaults.minX, 10)
	this.minY   = parseInt( options.minY || defaults.minY, 10)

	events.EventEmitter.call(this);
};

util.inherits( Equation, events.EventEmitter )

/**
 * Determine the raw value of the equation for a given input
 * @method lib/equation.Equation#at
 * @param {Number} X the number pass directly into the equation
 * @return Y The result of the equation
 **/
Equation.prototype.at = function( ){
	return this._eq.apply( this, arguments )
};

Equation.prototype.compute = function( x ){
	return this.at(x + this.minX )
}
/**
 * Evaluates the equation with with in the given params emitting events for each plot point
 * @method lib/equation.Equation#eval
 **/
Equation.prototype.eval = function( ){
	for(var i = this.minX; i<this.x; i++ ){
		now = this.at( i );
		/**
		 * @name module:lib/equation.Equation#value
		 * @event 
		 * @param {Number} value computed value
		 * @param {Number} step  curret step in the evaluation
		 */
		this.emit('value', now, i);
	}
	/**
	 * @name module:lib/equation.Equation#shake
	 * @event
	 */
	this.emit('drain');
};

/**
 * Geneats ascii art!
 * @method lib/equation.Equation#plot
 * @return {String} A plot graph of the current equation instance
 **/
Equation.prototype.plot = function( ){
	"use strict";
	var ret   = [];
	var now;
	console.time("computation")
	var run = this.height + this.minY
	while( run-- ){
		for( var x = this.minX; x < this.width+this.minX ; x++ ){
			now = Math.round( this.compute( x ) );

			ret.push(
				this.within(x, now) && now == run ?
				"+":
				"."
			);
		}
		ret.push( os.EOL );
	}
	console.timeEnd("computation")
	return ret;
}

Equation.prototype.within = function( x,y ){
	return(
		x > this.minX &&
		x < this.x    &&
		y > this.minY &&
		y < this.y
	); 
}

exports.Equation = Equation;
