#!/usr/bin/env node

var program = require('commander')
var equation = require("../lib/equation")
var readline = require("readline")
var defaults = require("../conf/defaults")
var fs = require('fs')
var path = require('path')
program
	.command("plot <equation>")
	.option('-x, --minx <x>', "minimum x value", defaults.minX)
	.option('-y, --miny <y>', "minimum y value", defaults.minY)
	.option('-X, --maxx <X>', "maximum x value", defaults.x)
	.option('-Y, --maxy <Y>', "maximum y value", defaults.y)
	.option('-H, --height <h>', "graph height", defaults.height)
	.option('-W, --width <w>', "graph width", defaults.width)
	.option('-o, --output <filename>')
	.action( function(expression, options ){
		var eq = new equation.Equation(
			expression ,{
				x:options.maxx
				,y:options.maxy
				,minY:options.miny
				,minX:options.minx
				,height:options.height
				,width:options.width
			}
		)
		var result = eq.plot().join("");
		console.log( result )
		if( options.output ){
			var fp = path.normalize( path.resolve( options.output ) );

			fs.writeFile(fp, result, {"encoding":"ascii"}, function(err){
				if( err ){
					console.log('unable to write file %s', fp)
					console.log(err.message)
				}

			})
		}
	})

program.parse( process.argv )