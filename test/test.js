var assert = require("assert")
var equation = require("../lib/equation")

describe('equation', function(){
  describe('#from()', function(){
    it('should return a function', function(){
    	var body = "(x/2)"
    	assert.equal( typeof equation.from( body ), "function" )
    });

    it('should throw when equation return non-numbers', function(){
    	var body = "NaN"
    	assert.throws(function(){
	    	aequation.from( body )
    	},Error )
    })

    it("should support JavaScript Globals", function(){
    	var body = "x*Math.PI"
    	var expect;
    	var result;
    	var procedure;
    	expect = 4 * Math.PI;
    	procedure = equation.from( body )
    	result = procedure( 4 );
    	assert.equal( expect, result)

    })

    it("should crunch it's own values", function(){
    	var body = "x+2";
    	var procedure = equation.from( body )
    	assert.strictEqual( procedure( 2 ), 4 )
    	assert.strictEqual( procedure( 100 ), 102 )
    	assert.strictEqual( procedure( -2 ), 0 )
    })
  })

  describe("Equation", function(){
  	it("should emit events", function( done ){
  		var eq = new equation.Equation("Math.pow(x,2)",{x:10,y:20});

  		eq.on('value', function(val){
  			assert( typeof val === "number", "should contain a number" )
  		});

  		eq.on('drain', done)

  		eq.eval();
  	})

  	it('should crunch its own numbers', function(){
  		var eq = new equation.Equation('Math.sin(x, 20)',{x:10,y:20} );

  		assert.equal( eq.at( 5 ), -0.9589242746631385)
  	})

    it("should compute a valid Y value", function(){
        var eq = new equation.Equation('Math.pow(2, 8 * (x-1))',{x:50,y:50} );

        assert.equal( eq.compute( 20 ) , 1.7948411796828674)
    })
  })
})